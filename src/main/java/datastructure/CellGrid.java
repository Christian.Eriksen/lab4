package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	int cols;
	int rows;
	CellState[][] grid;
	
	

    public CellGrid(int rows, int columns, CellState initialState) {
    	this.rows = rows;
    	this.cols = columns;
    	grid = new CellState[rows][columns];
    	for (int row=0; row<rows;row++) {
    		for (int col=0; col<columns;col++) {
    			grid[row][col] = initialState;
    		}
    	}
    	
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	if (( row >= 0 && row < this.numRows() ) && (column >= 0 && column < this.numColumns())) {
    		 this.grid[row][column] = element;
    	}
    	else {
    		throw new IndexOutOfBoundsException("Only input over 0 is allowd");
    	}
       
    }

    @Override
    public CellState get(int row, int column) {
    	if (( row >= 0 && row < this.numRows() ) && (column >= 0 && column < this.numColumns())) {
   		 return this.grid[row][column];
   	}
   	else {
   		throw new IndexOutOfBoundsException("Only input over 0 is allowd");
   	}
    }

    @Override
    public IGrid copy() {
    	IGrid newGrid = new CellGrid(this.numRows(), this.numColumns(), CellState.ALIVE);    	
    	for (int row=0; row<newGrid.numRows();row++) {
    		for (int col=0; col<newGrid.numColumns();col++) {
    			newGrid.set(row, col, this.get(row, col));
    		}
    	}
        return newGrid;
    }
    
}
